import React from 'react';
import { View, Text } from 'react-native';
import {Provider} from 'react-redux';
import storeRedux  from './src/redux/store';
import Route from './src/Route/Route';


const App = () => {
  return (
   
      <Provider store={storeRedux}>
        {/* <Route/> */}
        <Route/>
      </Provider>
    
  );
};

export default App;