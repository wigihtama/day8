const initialState = {
  DataFetchs: null,
};

const Fetchs = (state = initialState, action) => {
  switch (action.type) {
    case 'Home':
      return {
        ...state,
        DataFetchs: action.data,
      };
    default:
      return state;
  }
};

export default Fetchs;
