import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const Getone = nama => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  // kenapa dia return async dispatch karena dispatch nya harus di inisialisasi dulu

  return async dispatch => {
    var data = JSON.stringify({
      amount: 1000,
      sender: nama,
      target: 'BRI',
      type: 'BAYAR DULU NAPA',
    });

    var config = {
      method: 'POST',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        // ini function untuk menjalankan proses simpan

        dispatch({
          // tanda pengenal switch case nya
          type: 'LOGIN_SUCCESS',
          // data yang disimpan
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};
