import {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Getone} from '../redux/Action/Action';

const Home = () => {
  const [nama, setNama] = useState('ISDU');
  // cara kirim data ke action

  const dispatch = useDispatch();
  const GetAction = async () => {
    try {
      dispatch(Getone(nama));
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    GetAction();
  }, []);

  // cara ambil data dari reducer adalah
  const state = useSelector(state => state.Auth);
  console.log('ini auth lo', state);

  return (
    <View>
      <Text>Home</Text>
    </View>
  );
};

export default Home;
