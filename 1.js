import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const DataDiri = () => {
  const [nama, setNama] = useState('');
  const [umur, setUmur] = useState('');
  const [email, setEmail] = useState('');
  const [no, setNo] = useState('');
  const [alamat, setAlamat] = useState('');

  useEffect(() => {
    loadData();
  }, []);

  const saveData = async () => {
    try {
      await AsyncStorage.setItem('nama', ' woiii');
      await AsyncStorage.setItem('umur', '25');
      await AsyncStorage.setItem('email', 'woilahhh@gmail.com');
      await AsyncStorage.setItem('no', '0857572781827');
      await AsyncStorage.setItem('alamat', 'Jl. Jendral Sudirman No. 123');

      loadData();
    } catch (error) {
      console.log(error);
    }
  };

  const loadData = async () => {
    try {
      const nama = await AsyncStorage.getItem('nama');
      const umur = await AsyncStorage.getItem('umur');
      const email = await AsyncStorage.getItem('email');
      const no = await AsyncStorage.getItem('no');
      const alamat = await AsyncStorage.getItem('alamat');

      if (
        nama !== null &&
        umur !== null &&
        email !== null &&
        no !== null &&
        alamat !== null
      ) {
        setNama(nama);
        setUmur(umur);
        setEmail(email);
        setNo(no);
        setAlamat(alamat);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const removeData = async () => {
    try {
      await AsyncStorage.removeItem('nama');
      await AsyncStorage.removeItem('umur');
      await AsyncStorage.removeItem('email');
      await AsyncStorage.removeItem('no');
      await AsyncStorage.removeItem('alamat');

      setNama('');
      setUmur('');
      setEmail('');
      setNo('');
      setAlamat('');
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View>
      <Text>Nama: {nama}</Text>
      <Text>Umur: {umur}</Text>
      <Text>Email: {email}</Text>
      <Text>No Handphone: {no}</Text>
      <Text>Alamat: {alamat}</Text>
      <Button title="Simpan Data" onPress={saveData} />
      <Button title="Hapus Data" onPress={removeData} />
    </View>
  );
};

// export default DataDiri;





import {
    View,
    Text,
    SafeAreaView,
    TextInput,
    KeyboardAvoidingView,
    ScrollView,
    TouchableOpacity,
  } from 'react-native';
  import React, {useState} from 'react';
  import AsyncStorage from '@react-native-async-storage/async-storage';
  
  const App = () => {
    const [nama, setNama] = useState('');
    const [umur, setUmur] = useState('');
    const [email, setEmail] = useState('');
    const [number, setNumber] = useState('');
    const [alamat, setAlamat] = useState('');
  
    var user = {
      nama: nama,
      umur: umur,
      email: email,
      number: alamat,
      alamat: number,
    };
    const Simpan = async (value: any) => {
      await AsyncStorage.setItem('store', JSON.stringify(value));
    };
  
    const ambil = async () => {
      try {
        const data = await AsyncStorage.getItem('store');
        console.log(JSON.parse(data));
        const parsing = JSON.parse(data);
        setNama(parsing.nama), setUmur(parsing.umur);
        setEmail(parsing.email), setNumber(parsing.number);
        setAlamat(parsing.alamat);
      } catch (e) {
        console.log(e);
      }
    };
  
    return (
      <KeyboardAvoidingView>
        <ScrollView>
          <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            <View
              style={{
                backgroundColor: '#eeeee4',
                height: Platform.OS == 'android' ? 580 : 690,
                borderRadius: 25,
                marginLeft: 10,
                marginRight: 10,
                marginTop: 10,
              }}>
              <TextInput
                style={{color: 'black', padding: 20}}
                placeholder="Name"
                placeholderTextColor={'black'}
                value={nama}
                onChangeText={e => {
                  setNama(e);
                }}></TextInput>
              <TextInput
                style={{color: 'black', padding: 20}}
                placeholder="Age"
                placeholderTextColor={'black'}
                value={umur}
                onChangeText={e => {
                  setUmur(e);
                }}></TextInput>
              <TextInput
                style={{color: 'black', padding: 20}}
                placeholder="email"
                placeholderTextColor={'black'}
                value={email}
                onChangeText={e => {
                  setEmail(e);
                }}></TextInput>
              <TextInput
                style={{color: 'black', padding: 20}}
                placeholder="Nomor HP"
                placeholderTextColor={'black'}
                value={number}
                onChangeText={e => {
                  setNumber(e);
                }}></TextInput>
              <TextInput
                style={{color: 'black', padding: 20}}
                placeholder="Alamat"
                placeholderTextColor={'black'}
                value={alamat}
                onChangeText={e => {
                  setAlamat(e);
                }}></TextInput>
              <TouchableOpacity
                onPress={() => {
                  Simpan(user);
                }}>
                <Text>KONFIRMASI</Text>
              </TouchableOpacity>
              <Text style={{marginLeft: 15, padding: 5,}}>Nama : {nama}</Text>
              <Text style={{marginLeft: 15, padding: 5,}}>Umur : {umur}</Text>
              <Text style={{marginLeft: 15, padding: 5,}}>Email : {email}</Text>
              <Text style={{marginLeft: 15, padding: 5,}}>Nomor HP : {number}</Text>
              <Text style={{marginLeft: 15, padding: 5,}}>Alamat : {alamat}</Text>
              <TouchableOpacity
                onPress={() => {
                  ambil();
                }}>
                <Text>Ambil Data</Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  };
  
  export default App;
